package br.com.ticket.enums;

/**
 * @author Paulo R. Kraemer <paulork10@gmail.com>
 */
public enum TipoTicket {

    IDOSO("Idoso"), CRIANCA("Criança"), ESTUDANTE("Estudante");
    
    private final String label;
    
    TipoTicket(String label){
        this.label = label;
    }
    
    public String getLabel(){
        return this.label;
    }
    
}
