package br.com.ticket.entidade;

import br.com.ticket.enums.TipoTicket;

/**
 * @author Paulo R. Kraemer <paulork10@gmail.com>
 */
public abstract class Ticket {

    private TipoTicket tipo;
    private Double valor;
    private Double desconto; // Porcentagem de desconto
    
    public Ticket(TipoTicket tipo, Double valor) {
        this.tipo = tipo;
        this.valor = valor;
    }

    public TipoTicket getTipo() {
        return tipo;
    }

    public void setTipo(TipoTicket tipo) {
        this.tipo = tipo;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }
    
    
    public Double getValorTicket(){
        return this.valor-(this.valor*this.desconto);
    }
    
}
