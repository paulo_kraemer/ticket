package br.com.ticket.entidade;

import br.com.ticket.enums.TipoTicket;
import br.com.ticket.service.DescontoService;

/**
 * @author Paulo R. Kraemer <paulork10@gmail.com>
 */
public class TicketEstudante extends Ticket {
    
    private boolean carteirinha = false;
    private DescontoService service = new DescontoService();

    public TicketEstudante() {
        super(TipoTicket.ESTUDANTE, 8.0);
    }
    
    public TicketEstudante(boolean carteirinha){
        super(TipoTicket.ESTUDANTE, 8.0);
        this.carteirinha = carteirinha;
    }
    
    public boolean temCarteirinha() {
        return carteirinha;
    }

    public void setCarteirinha(boolean carteirinha) {
        this.carteirinha = carteirinha;
    }

    @Override
    public Double getValorTicket(){
        if(carteirinha){
            return super.getValor() - (super.getValor()*service.getDescontoCarteirinha());
        } else {
            return super.getValorTicket();
        }
    }

}
