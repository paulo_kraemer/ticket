package br.com.ticket.entidade;

import br.com.ticket.enums.TipoTicket;
import java.time.DayOfWeek;
import java.util.Objects;

/**
 * Criada para representar uma entidade que guardaria os registros de descontos
 * 
 * @author Paulo R. Kraemer <paulork10@gmail.com>
 */
public class Desconto {

    private DayOfWeek diaDaSemana;
    private TipoTicket tipo;
    private Double desconto; // Porcentagem de desconto

    public Desconto() {
    }

    public Desconto(DayOfWeek diaDaSemana, TipoTicket tipo, Double desconto) {
        this.diaDaSemana = diaDaSemana;
        this.tipo = tipo;
        this.desconto = desconto;
    }

    public DayOfWeek getDiaDaSemana() {
        return diaDaSemana;
    }

    public void setDiaDaSemana(DayOfWeek diaDaSemana) {
        this.diaDaSemana = diaDaSemana;
    }

    public TipoTicket getTipo() {
        return tipo;
    }

    public void setTipo(TipoTicket tipo) {
        this.tipo = tipo;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    @Override
    public String toString() {
        return "Desconto{" + "diaDaSemana=" + diaDaSemana + ", tipo=" + tipo + ", desconto=" + desconto + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.diaDaSemana);
        hash = 79 * hash + Objects.hashCode(this.tipo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Desconto other = (Desconto) obj;
        if (this.diaDaSemana != other.diaDaSemana) {
            return false;
        }
        if (this.tipo != other.tipo) {
            return false;
        }
        return true;
    }
    
}
