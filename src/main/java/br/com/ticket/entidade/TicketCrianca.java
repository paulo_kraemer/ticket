package br.com.ticket.entidade;

import br.com.ticket.enums.TipoTicket;

/**
 * @author Paulo R. Kraemer <paulork10@gmail.com>
 */
public class TicketCrianca extends Ticket {

    public TicketCrianca() {
        super(TipoTicket.CRIANCA, 5.5);
    }

}
