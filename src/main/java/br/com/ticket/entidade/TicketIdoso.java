package br.com.ticket.entidade;

import br.com.ticket.enums.TipoTicket;

/**
 * @author Paulo R. Kraemer <paulork10@gmail.com>
 */
public class TicketIdoso extends Ticket {
    
    public TicketIdoso() {
        super(TipoTicket.IDOSO, 6.0);
    }
    
}
