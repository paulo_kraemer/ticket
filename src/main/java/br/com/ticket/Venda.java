package br.com.ticket;

import br.com.ticket.entidade.TicketCrianca;
import br.com.ticket.entidade.TicketEstudante;
import br.com.ticket.entidade.Ticket;
import br.com.ticket.entidade.TicketIdoso;
import br.com.ticket.service.DescontoService;
import java.time.LocalDate;

/**
 * @author Paulo R. Kraemer <paulork10@gmail.com>
 */
public class Venda {

    // Usando CDI seria "@Inject"
    private DescontoService service = new DescontoService();
    
    public Double vendaEstudante(LocalDate date, boolean carteirinha){
        TicketEstudante e = new TicketEstudante(carteirinha);
        return vender(date, e);
    }
    
    public Double vendaCrianca(LocalDate date){
        TicketCrianca c = new TicketCrianca();
        return vender(date, c);
    }
    
    public Double vendaIdoso(LocalDate date){
        TicketIdoso i = new TicketIdoso();
        return vender(date, i);
    }
    
    private Double vender(LocalDate date,Ticket ticket){
        if(ticket.getDesconto() == null){
            ticket.setDesconto(service.getDesconto((date != null ? date : LocalDate.now()), ticket.getTipo()));
        }
        return ticket.getValorTicket();
    }
    
    private Double vender(Ticket ticket){
        return vender(null, ticket);
    }
    
}
