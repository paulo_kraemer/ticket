package br.com.ticket.service;

import br.com.ticket.entidade.Desconto;
import br.com.ticket.enums.TipoTicket;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Paulo R. Kraemer <paulork10@gmail.com>
 */
public class DescontoService {

    /**
     * Numa aplicação web temos o Front-End e o Back-End... e o Back-End pode
     * ser dividido entre Controllers e Services, além claro de DAO(s) e/ou
     * classes utilitárias...
     *
     * A idéia dessa classe é simular um comportamento parecido com o de uma
     * Service, que faz a manipulação dos dados retornados pela DAO (ou faz-se
     * consultas mais complexas diretamente nela) e que retorna algo para a 
     * Controller, a qual responde ao Front...
     */
    private final List<Desconto> DESCONTOS = new ArrayList<>();
    private final Double DESCONTO_CART = 0.35;

    /**
     * Simula os dados no banco de dados. Não está sendo representado feriados aqui
     */
    public DescontoService() {
        DESCONTOS.add(new Desconto(DayOfWeek.MONDAY, TipoTicket.IDOSO, 0.10));
        DESCONTOS.add(new Desconto(DayOfWeek.MONDAY, TipoTicket.ESTUDANTE, 0.10));
        DESCONTOS.add(new Desconto(DayOfWeek.MONDAY, TipoTicket.CRIANCA, 0.10));

        DESCONTOS.add(new Desconto(DayOfWeek.TUESDAY, TipoTicket.IDOSO, 0.15));
        DESCONTOS.add(new Desconto(DayOfWeek.TUESDAY, TipoTicket.ESTUDANTE, 0.05));
        DESCONTOS.add(new Desconto(DayOfWeek.TUESDAY, TipoTicket.CRIANCA, 0.15));

        DESCONTOS.add(new Desconto(DayOfWeek.WEDNESDAY, TipoTicket.IDOSO, 0.40));
        DESCONTOS.add(new Desconto(DayOfWeek.WEDNESDAY, TipoTicket.ESTUDANTE, 0.50));
        DESCONTOS.add(new Desconto(DayOfWeek.WEDNESDAY, TipoTicket.CRIANCA, 0.30));

        DESCONTOS.add(new Desconto(DayOfWeek.THURSDAY, TipoTicket.IDOSO, 0.30));
        DESCONTOS.add(new Desconto(DayOfWeek.THURSDAY, TipoTicket.ESTUDANTE, 0.30));
        DESCONTOS.add(new Desconto(DayOfWeek.THURSDAY, TipoTicket.CRIANCA, 0.00));

        DESCONTOS.add(new Desconto(DayOfWeek.FRIDAY, TipoTicket.IDOSO, 0.00));
        DESCONTOS.add(new Desconto(DayOfWeek.FRIDAY, TipoTicket.ESTUDANTE, 0.00));
        DESCONTOS.add(new Desconto(DayOfWeek.FRIDAY, TipoTicket.CRIANCA, 0.10));
        
        DESCONTOS.add(new Desconto(DayOfWeek.SATURDAY, TipoTicket.IDOSO, 0.05));
        DESCONTOS.add(new Desconto(DayOfWeek.SATURDAY, TipoTicket.ESTUDANTE, 0.05));
        DESCONTOS.add(new Desconto(DayOfWeek.SATURDAY, TipoTicket.CRIANCA, 0.05));
        
        DESCONTOS.add(new Desconto(DayOfWeek.SUNDAY, TipoTicket.IDOSO, 0.05));
        DESCONTOS.add(new Desconto(DayOfWeek.SUNDAY, TipoTicket.ESTUDANTE, 0.05));
        DESCONTOS.add(new Desconto(DayOfWeek.SUNDAY, TipoTicket.CRIANCA, 0.05));
    }
    
    /**
     * Simula uma consulta no banco de dados
     */
    public Double getDesconto(TipoTicket tipo) {
        return DESCONTOS.stream()
                .filter(a -> a.getDiaDaSemana().equals(LocalDate.now().getDayOfWeek()) && a.getTipo().equals(tipo))
                .findAny()
                .get()
                .getDesconto();
    }

    public Double getDesconto(LocalDate date, TipoTicket tipo) {
        return DESCONTOS.stream()
                .filter(a -> a.getDiaDaSemana().equals(date.getDayOfWeek()) && a.getTipo().equals(tipo))
                .findAny()
                .get()
                .getDesconto();
    }

    public Double getDescontoCarteirinha() {
        return DESCONTO_CART;
    }

}
