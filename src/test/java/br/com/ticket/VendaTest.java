package br.com.ticket;

import java.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Paulo R. Kraemer <paulork10@gmail.com>
 */
public class VendaTest {
    
    private final Venda instance = new Venda();
    
    public VendaTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    /**
     * Está sendo testado apenas um dia (sábado). Teria que testar todos.
     */

    @Test
    public void vendaEstudanteComCarteirinha() {
        System.out.println("Venda p/ estudante, com carteirinha...");
        LocalDate date = LocalDate.of(2016, 10, 1);// Sábado
        boolean carteirinha = true;
        Double expResult = 5.2;
        Double result = instance.vendaEstudante(date, carteirinha);
        assertEquals(expResult, result);
    }
    
    @Test
    public void vendaEstudanteSemCarteirinha() {
        System.out.println("Venda p/ estudante, sem carteirinha...");
        LocalDate date = LocalDate.of(2016, 10, 1);// Sábado
        boolean carteirinha = false;
        Double expResult = 7.6;
        Double result = instance.vendaEstudante(date, carteirinha);
        assertEquals(expResult, result);
    }

    @Test
    public void vendaCrianca() {
        System.out.println("Venda para criança...");
        LocalDate date = LocalDate.of(2016, 10, 1);// Sábado
        Double expResult = 5.225;
        Double result = instance.vendaCrianca(date);
        assertEquals(expResult, result);
    }
    
    @Test
    public void vendaIdoso() {
        System.out.println("Venda para idoso...");
        LocalDate date = LocalDate.of(2016, 10, 1);// Sábado
        Double expResult = 5.7;
        Double result = instance.vendaIdoso(date);
        assertEquals(expResult, result);
    }
    
}
